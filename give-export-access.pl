#!/usr/bin/perl
#
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

use strict;
use Digest::MD5  qw(md5_hex);
use Fcntl qw(:flock);
use String::MkPasswd qw(mkpasswd);  # libstring-mkpasswd-perl
use Authen::Htpasswd; # libauthen-htpasswd-perl
use File::Temp qw/tempdir/;

# public IP to generate proper URL
my $public_IP = "IP_UNSET";

my @required = ("openssl", "invoke-rc.d");
for (@required) {
    my $bin = `which openssl`;
    chomp($bin);
    die "$bin is required, exit" unless -x $bin;
}

# silently forbid concurrent runs
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

my $debug = "0";

my $srv = "/srv";
my $nginxmain = "/etc/nginx/sites-available/export";
my $nginxdir = "/etc/nginx/export.d";
my $ssldir = "/etc/ssl";
my $updated = "0";

# generate main conffile if necessary
unless (-e $nginxmain) {
    print "+ $nginxmain\n" if $debug;
    open(NGINXMAIN, "> $nginxmain");
    print NGINXMAIN "server {
  listen 443;
  ssl on;
  ssl_session_timeout 5m;
  # dirty snakeoil certificate
  ssl_certificate  $ssldir/certs/export.crt;
  ssl_certificate_key $ssldir/private/export.key;

  # allow directory index
  autoindex on;
  autoindex_localtime on;

  location / {
    deny all;
  }

  include $nginxdir/*.conf;
}";
    close(NGINXMAIN);
    $updated++;
}

# generate ssl certs if necessary (require openssl to be installed)
unless (-e "$ssldir/certs/export.crt" and -e "$ssldir/private/export.key") {
    print "+ CRT/key" if $debug;
    my $tempdir = tempdir(CLEANUP => 1);
    # generate key (passphase will be removed later)
    `openssl genrsa -des3 -out $tempdir/export.key 2048`;
    # generate certificate signing request (CSR)
    `openssl req -new -key $tempdir/export.key -out $tempdir/export.csr`;
    # remove passphrase
    `cp $tempdir/export.key $tempdir/export.key.org`;
    `openssl rsa -in $tempdir/export.key.org -out $tempdir/export.key`;
    # generate final certificate (CRT)
    `openssl x509 -req -days 7300 -in $tempdir/export.csr -signkey $tempdir/export.key -out $tempdir/export.crt`;
    # move to proper place
    `cp -v $tempdir/export.crt $ssldir/certs/`;
    `cp -v $tempdir/export.key $ssldir/private/`;
    $updated++;
}


chdir($srv) or die "Unable to enter $srv. Exit";
# go through the list of directories
my %validdir;
opendir(SRV, $srv);
while (defined(my $dir = readdir(SRV))) {
    # silently ignores anything but standard directories
    next unless -d $dir;
    next if $dir eq "." or $dir eq "..";

    print "$dir... " if $debug;
    
    # each dir is identified with its md5
    my $md5 = md5_hex($dir);
    
    # register that it is valid
    $validdir{$md5} = "1";

    # check if there is already a relevant conffile, if not, build it
    unless (-e "$nginxdir/$md5.conf") {
	print "$md5.conf " if $debug;
	
	# create the conffile
	open(CONFFILE, "> $nginxdir/$md5.conf");
	print CONFFILE "location ~ ^/$md5(.*) {
  auth_basic \"\";
  auth_basic_user_file $nginxdir/$md5.htpasswd;
  root \"$srv/$dir\";
  try_files \$1 \$1/ index.html;
}";
	close(CONFFILE);
	$updated++;
    }

    # check if there is already a password information file for the user
    #  (removing any of the password related files allows to reset
    # password)
    unless (-e "$srv/$dir/PASS.txt" and -e "$nginxdir/$md5.htpasswd") {
	print "PASS, htpasswd " if $debug;
	
	# otherwise generate user:password pair
	# (no special characters so it can be pass in the URL)
	my $user = mkpasswd( 
	    -length     => 12,
	    -minnum     => 0,
	    -minlower   => 12,
	    -minupper   => 0,
	    -minspecial => 0,
	    );
	my $password = mkpasswd(
	    -length     => 12,
	    -minnum     => 4,
	    -minlower   => 4,
	    -minupper   => 4,
	    -minspecial => 0,	
	    );
	
	# create the password file
	my $pwfile = Authen::Htpasswd->new("$nginxdir/$md5.htpasswd");
	$pwfile->add_user($user, $password);
			
	# store the information for the user
	open(INFOFILE, "> $srv/$dir/PASS.txt");
	print INFOFILE "https://$user:$password\@$public_IP/$md5/";
	close(INFOFILE);
	
    }
    print "OK\n" if $debug;
    
}
closedir(SRV);

# remove outdated dirs
opendir(CONF, $nginxdir);
while (defined(my $file = readdir(CONF))) {
    # silently ignores anything but files
    next unless -f "$nginxdir/$file";

    # if .conf, make sure relevant md5 is still valid
    if ($file =~ /^(.*)\.conf$/ or $file =~ /^(.*)\.htpasswd$/) {
	next if $validdir{$1};
	# if no valid, delete
	unlink("$nginxdir/$file");
	$updated++;
	print "- $file\n" if $debug;
    }
}
closedir(CONF);

# reload nginx config if updated
system("invoke-rc.d nginx reload 2>/dev/null >/dev/null") if $updated;

# add README
unless (-e "/$srv/LISEZMOI.txt") {
    open(README, "> /$srv/LISEZMOI.txt");
    print README "Pour créer un espace de téléchargement (lecture seule, pas d'implication de sécurité), créer un dossier.\r
\r
Dans la minute, un fichier PASS.txt sera crée contenant son URL pour accès hors du réseau local.\r
\r
Lors de la connexion, le navigateur indiquera que le certificat n'est pas signé par une autorité de confiance : accepter le certificat néanmoins.\r
\r
(note : utilisateur et mot-de-passe sont intégrés à l'URL sous la forme https://utilisateur:motdepasse\@IP/chemin/)\r
";
    close(README);
}
