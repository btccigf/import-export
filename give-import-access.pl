#!/usr/bin/perl
#
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

use strict;
use Fcntl qw(:flock);
use File::Copy;
use String::MkPasswd qw(mkpasswd);  # libstring-mkpasswd-perl

# silently forbid concurrent runs
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

my $debug = "0";

# go in work directory
my $srv = "/srv";
chdir($srv) or die "Unable to enter $srv. Exit";

# get list of users known to the system for this activity
#  validusers flags:
#             0 = unknown
#             1 = directory exists
#             2 = registered on the system
#             3 = directory exists and registered on the system
my %validusers;
my %protectedusers;
my $group = "sshusers";   # group of importers
my $groupid = getgrnam($group);
die "$group gid not found, exit " unless $groupid;
my $localgroup = "smbusers";    # group to access data within
my %nonfreeuserid;
my $minuserid = "2000";
# identify know users
while (my @sysusers = getpwent()) {
    # register nonfree uid
    $nonfreeuserid{$sysusers[2]} = "1";
    
    # register account
    if ($sysusers[3] eq $groupid) {
	# specific to import access
        $validusers{$sysusers[0]} = "2";
    }
    else {
	# otherwise protected anyway
	$protectedusers{$sysusers[0]} = "1"
    }
}
    
# go through the list of subdirectories to create new access
opendir(SRV, $srv);
while (defined(my $dir = readdir(SRV))) {
    # silently ignores anything but standard directories
    next unless -d $dir;
    next if $dir eq "." or $dir eq "..";

    # each dir is a user

    # if it matches a user already known to the system (flag 2),
    # flag and skip it (nothing to do, it is already set up)
    if ($validusers{$dir} > 1) {
	$validusers{$dir}++;
	print "= $dir\n" if $debug;
	next;
    }

    # otherwise we need to register a new user: clean up the dir name
    # to valid unix user name
    my $user = $dir;
    # keep only alphanum
    $user =~ s/[^\p{PosixAlnum},]//g;
    # lower case and 28 char max
    $user = lc(substr($user, 0, 28));

    # make sure this user name is not used already, otherwise find him a new 
    # name (by suffixing a number)
    my $userbase = $user;
    my $i;
    while ($validusers{$user} or $protectedusers{$user}) {
	# loop until we find a unused user name
	$i++;
	$user = $userbase.$i;
    }

    # find the first available uid, starting from the known minuserid
    while ($nonfreeuserid{$minuserid}) {
	# loop until we find a free uid
	$minuserid++;
    }
    # if satisfied, register uid as non free anymore
    $nonfreeuserid{$minuserid} = 0;
        
    # now create user
    print "+ $user\n";
    my $password = mkpasswd();
    print "$password\n" if $debug;
    system("/usr/sbin/useradd",
	   "--base-dir=$srv",
	   "--password=".crypt($password, "Q9"),
	   "--gid=$groupid",
	   "--uid=$minuserid",
	   "--no-user-group",
	   "--no-create-home",
	   "--shell=/bin/true",
	   $user);
	   

    # set up home directory
    # (note that files belongs to the given user and the local group, not
    # to the general ssh users group: files are shared from ssh user to
    # local user, not to other distant users)
    my $userdir = "$srv/$user";
    print "/srv/$dir $userdir \n" if $debug;
    move($dir, $userdir) if $dir ne $userdir;
    open(PWFILE, "> $userdir/PASS.txt");
    print PWFILE $password;
    close(PWFILE);
    system("/bin/chown", 
	   "$user:$localgroup", 
	   "--recursive",
	   $userdir);
    system("/bin/chmod",
	   "770",
	   $userdir);
    
    # register user we are satisfied with with flag 1 (to avoid name clashes)
    $validusers{$user} = 1;
   
}
closedir(SRV);

# removed obsolete user: existing on the system but with no directory
foreach my $user (keys %validusers) {
    # must be still flag 2
    next unless $validusers{$user} eq "2";

    print "- $user\n";
    system("/usr/sbin/userdel",
	   $user);

}

# add README
unless (-e "/$srv/LISEZMOI.txt") {
    open(README, "> /$srv/LISEZMOI.txt");
    print README "Pour créer un espace d'envoi de fichier à distance (droit d'écriture, implication de sécurité), créer un dossier.\r
\r
(pour un simple espace de téléchargement, voir \\\\export plutôt)\r
\r
Dans la minute, le nom d'utilisateur sera vérifié (renommé si besoin) et sera créé un fichier PASS.txt contenant son mot-de-passe.\r
\r
Se connecter à distance ensuite en utilisant un client SFTP (Filezilla, etc).\r
  hôte : IP publique (voir http://ipleak.net par exemple)\r
  port : 22\r
  protocole : SFTP\r
  utilisateur : nom du dossier\r
  mot-de-passe : contenu de PASS.txt\r
\r
Pour supprimer l'accès, supprimer le dossier.\r
Pas de réinitialisation d'un compte possible, supprimer et créer un autre au besoin.\r
";
    close(README);
}
    
# EOF
